import express from "express";
import UserController from "./controllers/UserController";
const router = express.Router();

// C
router.post("/users", UserController.create);

// R 
router.get("/users", UserController.findAll);
 
// R 
router.get("/users/:userId", UserController.findOne);

// U 
router.put("/users/:userId", UserController.update);

// D
router.delete("/users/:userId", UserController.destroy); 

export { router };
