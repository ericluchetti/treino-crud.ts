import { Sequelize } from "sequelize";

export const db = new Sequelize(
    'bancodedados',
    'root',
    '',
    {
        dialect: 'mysql',
        host: 'localhost',
        port: 3306
    }
);